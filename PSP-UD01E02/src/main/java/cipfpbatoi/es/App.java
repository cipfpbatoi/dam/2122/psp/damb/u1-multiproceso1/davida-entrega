package cipfpbatoi.es;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class App {
    /**
     * Ha faltado mostrar la salida en caso de error.
     * 
     * El método main no debe devolver un error.
     */
    public static void main(String[] args) throws IOException {
        if (args.length <= 0) {
            System.err.println("Ha ocurrido un error al ejecutar el proceso hijo.\nPor favor introduce un parámetro para continuar");
            System.exit(-1);
        }

        List<String> argumentos = new ArrayList<>(Arrays.asList(args));
        try {
            Process procesoHijo = new ProcessBuilder(argumentos).start();
            /**
             * No se ha de dormir el proceso con thread.sleep si no
             * esperar con waitFor
             */
            TimeUnit.SECONDS.sleep(2);
            InputStream is = procesoHijo.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            System.out.println("\nSalida del programa hijo " + Arrays.toString(args) + ":");
            System.out.println("-----------------------------------");
            String linea;

            File file = new File("output.txt");
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);

            while ((linea = br.readLine()) != null) {
                System.out.println(linea);
                bw.write("\n" + linea);
                /**
                 * Para esto, lo ideal es utilizar println si
                 * está disponible porque depende del sistema 
                 * operativo es uno u otro.
                 */
            }

            bw.close();
        } catch
        (IOException ex) {
            System.err.println("Hubo un error en tiempo de ejcución");
            System.exit(-1);
        } catch (InterruptedException ex) {
            System.err.println("El proceso fue interrumpido");
            System.exit(-1);
        }
    }
}


