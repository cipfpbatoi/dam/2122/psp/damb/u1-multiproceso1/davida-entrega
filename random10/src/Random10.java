import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class Random10 {
    public static void main(String[] args) {

        /**
         * Random10 Debería tener un bucle para ir leyendo,
         * en este caso, sólo leerá una vez.
         */
        Scanner sc = new Scanner(System.in);

        System.out.println("Escribe algo y me inventaré un número del 1 al 10. Escribe stop para salir");
        String cadenaUsuario = sc.nextLine();
        int numeroAleatorio = (int) (Math.random() * 10);
        System.out.println("Toma un número: " + numeroAleatorio);
    }
//    InputStream is = new InputStream() {
//
//    OutputStream os = new OutputStream(numeroAleatorio) {
//    }
}
