import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejercicio2 {
    public static void main(String[] args) {

        String comando = "java -jar ../random10/out/artifacts/random10_jar/random10.jar"; //ruta del otro programa o proyecto
        ProcessBuilder pb = new ProcessBuilder(comando.split(" ")); // creamos processbuilder con la lsita de argumentos

        try {
            Process random10 = pb.start(); //iniciar proceso

            OutputStream os = random10.getOutputStream(); //envío de palabras al proceso
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner random10sc = new Scanner(random10.getInputStream()); //Scanner lee el outputstream del otro programa
            Scanner sc = new Scanner(System.in);
            String linea = sc.nextLine();

            while (! linea.equals("stop")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                System.out.println(random10sc.nextLine());
                linea = sc.nextLine();
            }

        } catch (IOException ex) {
            Logger.getLogger(Ejercicio2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
